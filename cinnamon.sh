#!/bin/bash
##################################################################################################
##                                                                                              ##
##     ...............        .....         Cinnamon-Desktop-Installation                       ##
##    .doXMMMMxo;oMMMMO  .No KMMMMxdxOd.    Erst die Variablen anpassen!                        ##
##       kMMMM.   lMMMMO'N:  KMMMM.   dX    NICHT als root-user anmelden und ausführen!         ##
##       kMMMM.    ,WMMMW,   KMMMM.   cN                                                        ##
##       kMMMM.     .NMX.    KMMMM:;:x0.    Bei Fragen, komm einfach in den Matrix-Space:       ##
##       kMMMM.      00      KMMMMc:,.      https://matrix.to/#/#sontypiminternet:matrix.org    ##
##       :dddd      ;o       ldddd              So'n Typ Im Internet (2022)                     ##
##                                                                                              ##
##################################################################################################

##################
##  VARIABLE    ##
##################

country=Germany     # Hier dein Land für Reflector eintragen. Auf englisch: Germany, Switzerland, Austria,...
aur_helper=paru     # Welchen AUR-Helper? Es gibt: paru, yay, pacaur, pikaur, trizen

##########################
##  INSTALLATIONSSCRIPT ##
##########################

sudo timedatectl set-ntp true
sudo hwclock --systohc

sudo reflector -c $country -a 12 --sort rate --save /etc/pacman.d/mirrorlist

sudo firewall-cmd --add-port=1025-65535/tcp --permanent
sudo firewall-cmd --add-port=1025-65535/udp --permanent
sudo firewall-cmd --reload

git clone https://aur.archlinux.org/$aur_helper.git
cd $aur_helper
makepkg -si --noconfirm

$aur_helper -S --noconfirm xviewer
$aur_helper -S --noconfirm xplayer
$aur_helper -S --noconfirm pix

#$aur_helper -S --noconfirm system76-power
#sudo systemctl enable --now system76-power
#sudo system76-power graphics integrated
#$aur_helper -S --noconfirm gnome-shell-extension-system76-power-git
#$aur_helper -S --noconfirm auto-cpufreq
#sudo systemctl enable --now auto-cpufreq

sudo pacman -S xorg lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings cinnamon firefox simplescreenrecorder arc-gtk-theme arc-icon-theme obs-studio vlc xed xreader metacity gnome-shell archlinux-wallpaper cinnamon-translation gnome-terminal

sudo flatpak install -y spotify
sudo flatpak install -y kdenlive

sudo systemctl enable lightdm
/bin/echo -e "\e[1;32mNEUSTART IN 5..4..3..2..1..\e[0m"
sleep 5
sudo reboot
