#!/bin/bash
##################################################################################################
##                                                                                              ##
##     ...............        .....         BSPWM-Installation                                  ##
##    .doXMMMMxo;oMMMMO  .No KMMMMxdxOd.    Erst die Variablen anpassen!                        ##
##       kMMMM.   lMMMMO'N:  KMMMM.   dX    NICHT als root-user anmelden und ausführen!         ##
##       kMMMM.    ,WMMMW,   KMMMM.   cN                                                        ##
##       kMMMM.     .NMX.    KMMMM:;:x0.    Bei Fragen, komm einfach in den Matrix-Space:       ##
##       kMMMM.      00      KMMMMc:,.      https://matrix.to/#/#sontypiminternet:matrix.org    ##
##       :dddd      ;o       ldddd              So'n Typ Im Internet (2022)                     ##
##                                                                                              ##
##################################################################################################

##################
##  VARIABLE    ##
##################

country=Germany     # Hier dein Land für Reflector eintragen. Auf englisch: Germany, Switzerland, Austria,...
aur_helper=paru-bin     # Welchen AUR-Helper? Es gibt: paru, yay, pacaur, pikaur, trizen

##########################
##  INSTALLATIONSSCRIPT ##
##########################
sudo timedatectl set-ntp true
sudo hwclock --systohc

sudo reflector -c $country -a 12 --sort rate --save /etc/pacman.d/mirrorlist
sudo pacman -Sy

sudo firewall-cmd --add-port=1025-65535/tcp --permanent
sudo firewall-cmd --add-port=1025-65535/udp --permanent
sudo firewall-cmd --reload
# sudo virsh net-autostart default

git clone https://aur.archlinux.org/$aur_helper.git
cd $aur_helper/
makepkg -si --noconfirm

$aur_helper -S --noconfirm lightdm-slick-greeter 
$aur_helper -S --noconfirm lightdm-settings
$aur_helper -S --noconfirm polybar
$aur_helper -S --noconfirm nerd-fonts-iosevka
$aur_helper -S --noconfirm ttf-icomoon-feather

#$aur_helper -S --noconfirm system76-power
#sudo systemctl enable --now system76-power
#sudo system76-power graphics integrated
#$aur_helper -S --noconfirm gnome-shell-extension-system76-power-git
#$aur_helper -S --noconfirm auto-cpufreq
#sudo systemctl enable --now auto-cpufreq

echo "MAIN PACKAGES"

sleep 5

sudo pacman -S xorg light-locker lightdm bspwm sxhkd firefox rxvt-unicode picom nitrogen lxappearance dmenu nautilus arandr simplescreenrecorder alsa-utils pulseaudio alsa-utils pulseaudio-alsa pavucontrol arc-gtk-theme arc-icon-theme obs-studio vlc dina-font tamsyn-font bdf-unifont ttf-bitstream-vera ttf-croscore ttf-dejavu ttf-droid gnu-free-fonts ttf-ibm-plex ttf-liberation ttf-linux-libertine noto-fonts ttf-roboto tex-gyre-fonts ttf-ubuntu-font-family ttf-anonymous-pro ttf-cascadia-code ttf-fantasque-sans-mono ttf-fira-mono ttf-hack ttf-fira-code ttf-inconsolata ttf-jetbrains-mono ttf-monofur adobe-source-code-pro-fonts cantarell-fonts inter-font ttf-opensans gentium-plus-font ttf-junicode adobe-source-han-sans-otc-fonts adobe-source-han-serif-otc-fonts noto-fonts-cjk noto-fonts-emoji ttf-font-awesome awesome-terminal-fonts archlinux-wallpaper rofi playerctl scrot obs-studio dunst pacman-contrib

sudo flatpak install -y spotify
sudo flatpak install -y kdenlive

sudo systemctl enable lightdm

mkdir -p ~/.config/{bspwm,sxhkd,dunst}

install -Dm755 /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/bspwmrc
install -Dm644 /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd/sxhkdrc

printf "\e[1;32mERST DIE KONFIGURATIONEN ANPASSEN, BEVOR DU NEUSTARTEST!\e[0m"

